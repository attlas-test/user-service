const { ui } = require('eroc')
const moment = require('moment')
const Block = require('../../models/Block')
const eth = require('../../eth')

module.exports = ui('eth', {

    table: async (req, res, next) => {
        const address = req.gp('address', undefined, /^0x[0-9a-f]{40}$/)
        const limit = req.gp('limit', 10, Number)
        const offset = req.gp('offset', 0, Number)
        const draw = req.gp('draw', 0)

        const current = moment().set({ minute: 0, second: 0 })
        const rows = []
        const promises = []

        // Init rows
        for(let i = offset; i < offset + limit; i++) {
            rows.push({
                time: moment(current).add({ hour: -i })
            })
        }

        // Prepare the blocks 
        const blocks = await Block
            .find({
                time: {
                    $lte: rows[0].time,
                    $gte: rows[rows.length - 1].time - 300000,
                }
            })
            .sort({ time: -1 })
            .lean()

        // Mapping time - block
        for (const row of rows) {
            /**
             * Get the first block in a non-decreasing time list that is less than or equal to "row.time"
             * It represents the balance at "row.time"
             * Ex. blocks=[5.1, 4.9, 4, 2, 1] and row.time=5 ==> pick 4.9
             */

            const block = blocks.find((block) => {
                return block.time <= row.time
            })

            row.blockTime = block?.time
            row.blockNumber = block?.number

            if (row.blockNumber) {
                promises.push(new Promise((resolve, reject) => {
                    eth.getBalance(address, row.blockNumber)
                        .then((balance) => {
                            row.balance = balance
                            resolve()
                        })
                        .catch(reject)
                }))
            }

        }

        // Wait for all the promises to be fulfilled
        await Promise.all(promises)

        // 30 days, 1 hour per row
        const total = 720

        return res.success(rows, { meta: { total, draw } })
    },
})
