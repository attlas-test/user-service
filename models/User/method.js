const { config, jwt } = require('eroc')
const crypto = require('crypto')
const schema = require('./schema')

schema.methods.profile = function () {
    const profile = this.toObject()

    delete profile.hashedPassword
    delete profile.salt

    return profile
}

schema.methods.genHashedPassword = function (password) {
    check(this.salt, 'Missing user.salt')
    check(config.password_pepper, 'Missing config.password_pepper')

    //! Only sha256 for fast demo
    return crypto.createHash('sha256')
        .update(password)
        .update(this.salt)
        .update(config.password_pepper)
        .digest('hex')
}

schema.methods.checkPassword = async function (password) {
    return this.hashedPassword === this.genHashedPassword(password)
}

schema.methods.getTokenData = function () {
    const data = {
        _id: this._id,
        status: this.status,
        username: this.username,
    }

    return data
}

schema.methods.getToken = function (extra) {
    return jwt.sign(Object.assign(this.getTokenData(), extra))
}
