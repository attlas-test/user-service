const { mongoose } = require('eroc')
const schema = require('./schema')

require('./method')
require('./static')
require('./hook')

schema.index({ status: 1 })

module.exports = mongoose.model('User', schema)
