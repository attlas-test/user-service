const { schedule } = require('eroc')
const eth = require('../eth')
const Block = require('../models/Block')

// Block at 26/11/2022
const START_BLOCK = 16055800
let running = false

/**
 * Retrieve to the latest block
 */
const crawl = async () => {
    console.info('tasks.block: Start crawling')
    running = true

    try {
        const last = await Block.findOne().sort({ time: -1 }).lean()
        const start = last?.number + 1 || START_BLOCK
        let count = 0

        const tick = async (bn) => {
            const block = await eth.getBlock(bn)

            if (!block) {
                running = false
                return console.info(`tasks.block: Stop crawling - count=${count}`)
            }

            Block.updateOne(
                {
                    number: block.number,
                },
                {
                    time: new Date(block.timestamp * 1000),
                },
                {
                    upsert: true,
                },
            ).exec()

            count++

            if (count % 100 === 0) {
                console.info(`task.block: Insert ${count} block done, at ${block.number}`)
            }

            // Recursive call with async-context
            await tick(block.number + 1)
        }

        await tick(start)
    } catch (error) {
        console.error(error)
        running = false
    }
}

// Every 5 minutes, re-run crawl
schedule.add('0 */5 * * * *', () => {
    if (!running) {
        crawl().catch(console.error)
    }
})

crawl().catch(console.error)
