const { config, redis } = require('eroc')
const Device = require('./models/Device')

const boot = async () => {
    await config.deferred.config

    if (config.env !== 'local-') {
        const limit = 1000
        let total = 0

        console.log(`boot: Start submiting device_tiat`)

        const tick = async (offset = 0) => {
            const devices = await Device
                .find()
                .skip(offset)
                .limit(limit)
                .select({ tiat: 1 })
                .lean()

            if (!devices.length) {
                return done()
            }

            const fields = []

            devices.forEach((d) => {
                fields.push(d._id.toString())
                fields.push(d.tiat?.toString() || '0')
            })

            await redis.cmd('hmset', 'device_tiat', ...fields)
            total += devices.length

            process.nextTick(tick, offset + limit)
        }

        const done = () => {
            console.log(`boot: Submit ${total} device_tiat 🧱`)
        }

        await redis.del('device_tiat')
        tick()
    }
}

module.exports = boot
