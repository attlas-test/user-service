const { mongoose, redis } = require('eroc')

const Schema = mongoose.Schema

const schema = new Schema(
    {
        uuid: String,
        name: String,
        active: Date,
        tiat: Number,

        user: {
            _id: String,
            username: String,
        },

        location: {
            accuracy: Number,
            latitude: Number,
            longitude: Number,
        },
    },
    {
        timestamps: true,
    },
)

schema.methods.updateTiat = async function () {
    this.tiat = Math.floor(Date.now() / 1000)
    await redis.hset('device_tiat', this._id.toString(), this.tiat)
}

module.exports = mongoose.model('Device', schema)
