const { mongoose } = require('eroc')


const Schema = mongoose.Schema

const schema = new Schema(
    {
        number: Number,
        time: Date,
    },
    {
        versionKey: false,
    }
)

schema.index({ time: 1 })
schema.index({ number: 1 })

module.exports = mongoose.model('Block', schema)
