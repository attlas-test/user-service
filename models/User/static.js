const schema = require('./schema')
const constant = require('./constant')

Object.assign(schema.statics, constant)

schema.statics.create = async function (data) {
    data.salt = Math.random().toString(36).slice(2)

    const user = new this(data)

    if (data.password) {
        user.hashedPassword = user.genHashedPassword(data.password)
    }

    return user.save()
}
