const constant = {}

constant.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive',
    DELETED: 'deleted',
    WAIT_FOR_CONFIRMATION: 'wait_for_confirmation',
}

module.exports = constant
