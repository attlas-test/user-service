const { Router, redis } = require('eroc')
const Device = require('../../models/Device')

const router = Router()

router.post('/', async (req, res, next) => {
    const uuid = req.gp('uuid')
    const name = req.gp('name', '')
    const location = req.gp('location', '')
    
    const query = {
        uuid,
    }

    const tiat = Math.floor(Date.now() / 1000)

    const update = {
        active: new Date(),

        $setOnInsert: {
            tiat,
        }
    }

    if (req.u.user) {
        update.user = req.u.user
    }

    if (name !== null) {
        update.name = name
    }

    if (location !== null) {
        update.location = location
    }

    res.success()

    const result = await Device.updateOne(query, update, { upsert: true })

    if (result.upsertedId) {
        redis.hset('device_tiat', result.upsertedId.toString(), tiat)
    }
})

module.exports = router
