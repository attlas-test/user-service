/**
 * Không sử dụng queue
 * xử lý request tuần tự bằng promise.
 * Câu hỏi đề cập tại buổi phỏng vấn
 */

const { Router, util, request } = require('eroc')

const router = Router()

let pl = null

const check = async () => {
    if (!pl) {
        pl = util.deferred()
        return
    }

    await pl
    await check()
}

router.get('/', async (req, res, next) => {
    await check()
    
    // Doing async-IO
    await new Promise((resolve) => {
        setTimeout(resolve, 1000)
    })

    pl.resolve()
    pl = null

    console.info('Request done', req.gp('index'))
    res.success()
})

// setTimeout(() => {
//     request.get('user/interview-quest', { index: 1 }).catch(console.error)
//     request.get('user/interview-quest', { index: 2 }).catch(console.error)
//     request.get('user/interview-quest', { index: 3 }).catch(console.error)
//     request.get('user/interview-quest', { index: 4 }).catch(console.error)
//     request.get('user/interview-quest', { index: 5 }).catch(console.error)
// }, 200)

module.exports = router