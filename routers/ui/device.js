const { ui } = require('eroc')
const Device = require('../../models/Device')

module.exports = ui('device', {
    
    table: async (req, res, next) => {
        const limit = +req.gp('limit', 12)
        const offset = +req.gp('offset', 0)
        const draw = req.gp('draw', 0)
        const search = req.gp('search', '')

        const query = {
            'user._id': req.u.user._id,
        }

        if (search) {
            query.uuid = {
                $regex: search,
                $options: 'i',
            }
        }

        const data = await Device.find(query).sort({ _id: -1 }).skip(offset).limit(limit)
        const total = await Device.countDocuments(query)

        return res.success(data, { meta: { total, draw } })
    },
    
    logout: async (req, res, next) => {
        const _id = req.gp('_id')
        
        const device = await Device.findOne({ _id })
        check(device.user?._id == req.u.user._id, 'This device is not owned by you')
        
        await device.updateTiat()
        await device.save()

        return res.success()
    },
})
