const { Router } = require('eroc')
const User = require('../../models/User')
const Device = require('../../models/Device')

const router = Router()

router.post('/register', async (req, res, next) => {
    const username = req.gp('username')
    const password = req.gp('password')
    const uuid = req.gp('uuid')

    check(/^[a-zA-Z0-9._]{6,32}$/.test(username), 'Tên đăng nhập phải gồm 6 đến 32 chữ cái, số, ký tự . và _')
    check(password.length > 5 && password.length < 65, 'Độ dài mật khẩu phải từ 6 đến 64 kí tự')
    check((await User.findOne({ username })) === null, 'Tên đăng nhập đã được sử dụng')

    const raw = {
        username,
        password,
        platform: 'username',
    }

    const user = await User.create(raw)
    const device = await Device.findOne({ uuid })
    const profile = user.profile()
    const token = user.getToken({ device: device._id, tiat: device.tiat })

    res.u.cookie('token', token)
    return res.success(profile)
})

router.post('/login', async (req, res, next) => {
    const username = req.gp('username')
    const password = req.gp('password')
    const uuid = req.gp('uuid')
    
    const query = {
        status: {
            $ne: User.STATUS.DELETED,
        },
        username,
    }

    const user = await User.findOne(query)
    
    check(user && (await user.checkPassword(password)), 'Thông tin đăng nhập không đúng')
    check(user.status === User.STATUS.ACTIVE, 'Tài khoản đã bị khoá')
    
    const device = await Device.findOne({ uuid })
    const profile = user.profile()
    const token = user.getToken({ device: device._id, tiat: device.tiat })

    res.u.cookie('token', token)
    return res.success(profile)
})

module.exports = router
