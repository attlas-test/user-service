const { config } = require('eroc')
const Web3 = require('web3')

const MAX_RETRY = 5

const pools = config.eth_providers.map((provider) => {
    const web3 = new Web3(provider)
    const eth = web3.eth

    eth.__count = {
        total: 0,
        error: 0,
        time: 0,
    }

    return eth
})

const pick = () => {
    // TODO: handle pick by eth.__count
    return pools[Math.floor(Math.random() * pools.length)]
}

const proxy = new Proxy({}, {
    get(target, prop) {
        const wrap = async (...args) => {
            const handle = async (args, retry) => {
                const eth = pick()

                if (!eth[prop]) {
                    throw Error(`Not found web3.eth method ${prop}`)
                }

                // Retry with other provider before give up
                try {
                    eth.__count.total++
                    const now = Date.now()

                    return await eth[prop](...args).then((data) => {
                        eth.__count.time += Date.now() - now
                        return data
                    })
                } catch (error) {
                    eth.__count.error++

                    if (retry < MAX_RETRY) {
                        console.log('eth: retry', retry)

                        // Recursive call with async-context
                        return await handle(args, retry + 1)
                    }
                    
                    throw error
                }
            }

            return handle(args, 0)
        }

        return wrap
    },
})

module.exports = proxy
