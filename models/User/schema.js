const { mongoose } = require('eroc')
const constant = require('./constant')

module.exports = new mongoose.Schema(
    {
        status: {
            type: String,
            default: constant.STATUS.ACTIVE,
            enum: Object.values(constant.STATUS),
        },

        username: String,
        salt: String,
        hashedPassword: String,
    },
    {
        timestamps: true,
    },
)
