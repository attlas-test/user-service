const { ui } = require('eroc')
const Block = require('../../models/Block')
const eth = require('../../eth')

const TRANSFER_EVENT_SIGNATURE = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef' 

module.exports = ui('transfer', {

    fetch: async (req, res, next) => {
        const address = req.gp('address', undefined, /^0x[0-9a-f]{40}$/)

        // Get the nearest block less than 30 days
        const startBlock = await Block
            .findOne({
                time: {
                    // 30 days ago
                    $lt: Date.now() - 2592000000,
                }
            })
            .sort({ time: -1 })
            .lean()

        // Get all in/out transfer logs from startBlock to latest
        const [inLogs, outLogs] = await Promise.all([
            eth.getPastLogs({
                fromBlock: startBlock.number,
                topics: [
                    TRANSFER_EVENT_SIGNATURE,
                    null,
                    `0x${address.slice(2).padStart(64, '0')}`
                ]
            }),
            eth.getPastLogs({
                fromBlock: startBlock.number,
                topics: [
                    TRANSFER_EVENT_SIGNATURE,
                    `0x${address.slice(2).padStart(64, '0')}`,
                ]
            }),
        ])

        let logs = inLogs.concat(outLogs)
        
        // Remove duplicate transactions
        logs = logs.filter((log, index) => {
            return index === logs.findIndex((l) => l.transactionHash === log.transactionHash)
        })

        if (!logs.length) {
            return res.success([])
        }

        // Get all block-map for asssign transaction time
        const blocks = await Block.find({
            number: {
                $in: logs.map((log) => log.blockNumber)
            }
        })

        // Assign block time to transactions
        for (const log of logs) {
            log.time = blocks.find((block) => block.number === log.blockNumber)?.time
        }

        return res.success(logs)
    },
})
